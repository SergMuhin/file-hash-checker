const fs = require('fs').promises
const axios = require('axios')
const crypto = require('crypto')

async function readFile (filePath) {
  if (filePath.startsWith('http')) {
    const response = await axios.get(filePath)
    return response.data
  } else {
    return fs.readFile(filePath, 'utf-8')
  }
}

async function calculateHash (data) {
  const hash = crypto.createHash('sha256')
  hash.update(data)
  return hash.digest('hex')
}

async function compareHash (file, hashFile) {
  try {
    const fileContent = await readFile(file)
    const calculatedHash = await calculateHash(fileContent)

    const storedHash = await fs.readFile(hashFile, 'utf-8').then(data => data.trim())

    if (calculatedHash === storedHash) {
      console.log('Hashes match.')
      process.exit(0)
    } else {
      console.error('Hashes do not match.')
      process.exit(102)
    }
  } catch (err) {
    console.error(err.message)
    process.exit(101)
  }
}

async function main () {
  const filePath = process.argv[2] || process.env.FILES_PATH

  if (!filePath) {
    console.error('Please provide the file path or URL as an argument.')
    process.exit(1)
  }

  const hashFilePath = `${filePath}.sha256`

  try {
    await compareHash(filePath, hashFilePath)
  } catch (err) {
    console.error(err.message)
    process.exit(1)
  }
}

main()

module.exports = {
  readFile,
  calculateHash,
  compareHash
}
