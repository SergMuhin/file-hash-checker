const fs = require('fs').promises
const { readFile, calculateHash } = require('../index')
const filePath = process.env.FILES_PATH

jest.mock('axios')
fs.readFile = jest.fn()

describe('readFile', () => {
  test('should read local file', async () => {
    fs.readFile.mockResolvedValue('File content')
    const result = await readFile(filePath)

    expect(result).toEqual('File content')
  })
})

describe('calculateHash', () => {
  test('should calculate hash from data', async () => {
    const result = await calculateHash('Test data')

    expect(result).toEqual(expect.any(String))
  })
})
